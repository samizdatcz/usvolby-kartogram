var stateResults = {'AL': {'name': 'Alabama', 'win': 'trump'}, //trump or clinton
					'AK': {'name': 'Alaska', 'win': 'trump'},
					'AZ': {'name': 'Arizona', 'win': 'trump'},
					'AR': {'name': 'Arkansas', 'win': 'trump'},
					'CA': {'name': 'California', 'win': 'clinton'},
					'CO': {'name': 'Colorado', 'win': 'clinton'},
					'CT': {'name': 'Connecticut', 'win': 'clinton'},
					'DE': {'name': 'Delaware', 'win': 'clinton'},
					'DC': {'name': 'Washington DC', 'win': 'clinton'},
					'FL': {'name': 'Florida', 'win': 'trump'},
					'GA': {'name': 'Georgia', 'win': 'trump'},
					'HI': {'name': 'Hawaii', 'win': 'clinton'},
					'ID': {'name': 'Idaho', 'win': 'trump'},
					'IL': {'name': 'Illinois', 'win': 'clinton'},
					'IN': {'name': 'Indiana', 'win': 'trump'},
					'IA': {'name': 'Iowa', 'win': 'trump'},
					'KS': {'name': 'Kansas', 'win': 'trump'},
					'KY': {'name': 'Kentucky', 'win': 'trump'},
					'LA': {'name': 'Louisiana', 'win': 'trump'},
					'ME': {'name': 'Maine', 'win': 'clinton'},
					'MD': {'name': 'Maryland', 'win': 'clinton'},
					'MA': {'name': 'Massachusetts', 'win': 'clinton'},
					'MI': {'name': 'Michigan', 'win': 'trump'},
					'MN': {'name': 'Minnesota', 'win': 'clinton'},
					'MS': {'name': 'Mississippi', 'win': 'trump'},
					'MO': {'name': 'Missouri', 'win': 'trump'},
					'MT': {'name': 'Montana', 'win': 'trump'},
					'NE': {'name': 'Nebraska', 'win': 'trump'},
					'NV': {'name': 'Nevada', 'win': 'clinton'},
					'NH': {'name': 'New Hampshire', 'win': 'clinton'},
					'NJ': {'name': 'New Jersey', 'win': 'clinton'},
					'NM': {'name': 'New Mexico', 'win': 'clinton'},
					'NY': {'name': 'New York', 'win': 'clinton'},
					'NC': {'name': 'North Carolina', 'win': 'trump'},
					'ND': {'name': 'North Dakota', 'win': 'trump'},
					'OH': {'name': 'Ohio', 'win': 'trump'},
					'OK': {'name': 'Oklahoma', 'win': 'trump'},
					'OR': {'name': 'Oregon', 'win': 'clinton'},
					'PA': {'name': 'Pennsylvania', 'win': 'trump'},
					'RI': {'name': 'Rhode Island', 'win': 'clinton'},
					'SC': {'name': 'South Carolina', 'win': 'trump'},
					'SD': {'name': 'South Dakota', 'win': 'trump'},
					'TN': {'name': 'Tennessee', 'win': 'trump'},
					'TX': {'name': 'Texas', 'win': 'trump'},
					'UT': {'name': 'Utah', 'win': 'trump'},
					'VT': {'name': 'Vermont', 'win': 'clinton'},
					'VA': {'name': 'Virginia', 'win': 'clinton'},
					'WA': {'name': 'Washington', 'win': 'clinton'},
					'WV': {'name': 'West Virginia', 'win': 'trump'},
					'WI': {'name': 'Wisconsin', 'win': 'trump'},
					'WY': {'name': 'Wyoming', 'win': 'trump'}
				};

var colors = {
	'clinton': '#4393c3', 
	'trump': '#d6604d', 
	'na': '#bababa'
};

var winnerStatement = {
	'trump': 'Zvítězil zde republikán <span style="color: #d6604d;">Donald Trump</span>.',
	'clinton': 'Zvítězila zde demokratka <span style="color: #4393c3;">Hillary Clinton</span>.',
	'na': 'Výsledky v tomto státě zatím nejsou známé.'
};

var width = 1000;
var height = 500;

var svg = d3.select('#mapa').append('svg')
    .attr('width', width)
    .attr('height', height)

var infoBox = d3.select('#mapa').append('div')
		.attr('id', 'infobox')
		.html('Najetím myši vyberte stát USA.')
		.style('left', '700px')
		.style('bottom', '390px')

d3.json('./cartogram.json', function showData(error, tilegram) {
  var tiles = topojson.feature(tilegram, tilegram.objects.tiles)

  var transform = d3.geoTransform({
    point: function(x, y) {
      this.stream.point(x, -y)
    }
  })

  var path = d3.geoPath().projection(transform)

  var g = svg.append('g')
    .attr('transform', 'translate(0,' + (height - (height * 0.1)) + ')' + ' scale(' + (width / 2350) + ')')

  g.selectAll('.tiles')
    .data(tiles.features)
    .enter()
    .append('path')
    .style("fill", function(d, i) {
    	return colors[stateResults[d.properties.state].win]
    })
    .attr('d', path)

   var stateCodes = []
	tilegram.objects.tiles.geometries.forEach(function(geometry) {
	  if (stateCodes.indexOf(geometry.properties.state) === -1) {
	    stateCodes.push(geometry.properties.state)
	  }
	})

	console.log(stateCodes)

	// slozeni geometrie
	var stateBorders = stateCodes.map(function(code) {
	  return topojson.merge(
	    tilegram,
	    tilegram.objects.tiles.geometries.filter(function(geometry) {
	      return geometry.properties.state === code
	    })
	  )
	})

	// nakrelseni
	g.selectAll('path.border')
	  .data(stateBorders)
	  .enter()
	  .append('path')
	  .attr('d', path)
	  .attr('class', 'border')
	  .attr('fill', 'none')
	  .attr('stroke', '#e0e0e0')
	  .attr('stroke-width', 1)

	// hejbeseto
	g.selectAll('path').on('mouseover', function(e) {
		if (e.properties != undefined) {
			infoBox.html('<h3 class="infotitle">' + stateResults[e.properties.state].name + ' (volitelů: ' + e.properties.tilegramValue + ')</h3>'
				+ '<span class="infotext"> ' + winnerStatement[stateResults[e.properties.state].win] + ' </span>')
		} else {};
	});
});